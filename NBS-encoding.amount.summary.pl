#!/usr/bin/perl -w
use strict;
use Getopt::Long;

my $USAGE = <<USAGE;
Scripts: pre-NBS encoding summary

arguments: 

        -i           inputfile, merged NBS relevant domain/motif file
        -pfx         prefix
        -o           output file of NB-ARC encoding genes
        -r           outptu file of RPW8 encoding genes (new feature)
        
USAGE

#---------- parameters which should be defined in advance -----------
GetOptions(my $options = {},
                    "-i=s","-pfx=s","-o=s","-r=s"
           );

die "unable to open iputfile\n$USAGE" unless ($options->{i});

my $i = 0;
my %total = ();
my %gene  = ();

my $input     = $options->{i};
my $prefix    = ($options->{pfx}) ? $options->{pfx}: "default.";
my $out_nbs       = ($options->{o}) ? $options->{o}: $prefix."NBS.pre.candidates.lst" ;
my $out_rpw8  = ($options->{r}) ? $options->{r}: $prefix."RPW8.candidates.lst";
my $othertype = $prefix."other.type.candidates.txt";

open(IN,   "$input");
open(OTHER,">$othertype");
open(TYPE, ">$out_nbs");
open(RPW8, ">$out_rpw8");
while (<IN>) {
    chomp;
    $i++;
    # ignore the first line which is usually the title of each column.    
    next if ($i == 1); 
    
    my ($id,$len,$nbs,$lrr,$tir,$cc,$rpw,@other)  = split/\t/,$_;
    my $v_nbs = domain_detect($nbs);
    my $v_lrr = domain_detect($lrr);
    my $v_cc  = domain_detect($cc);
    my $v_tir = domain_detect($tir);
    my $v_rpw = domain_detect($rpw);

    
    if ($v_nbs == 1                                          and $v_lrr + $v_cc + $v_tir + $v_rpw           == 0) {
        $total{NBS}++;
        push(@{$gene{NBS}},$id);
    }
    
    elsif ($v_nbs == 1  and  $v_lrr == 1                     and $v_cc + $v_tir  + $v_rpw == 0) {
        $total{NL}++;
        push(@{$gene{NL}},$id);
    }
    
    elsif ($v_cc == 1  and  $v_nbs == 1  and $v_lrr == 1     and  $v_tir + $v_rpw == 0) {
        $total{CNL}++;
        push(@{$gene{CNL}},$id);
    }

    elsif ($v_tir == 1 and  $v_nbs == 1  and $v_lrr == 1     and $v_cc + $v_rpw == 0) {
        $total{TNL}++;
        push(@{$gene{TNL}},$id);
    }
    
    elsif ($v_tir == 1 and  $v_nbs == 1                      and $v_lrr + $v_cc + $v_rpw == 0) {
        $total{TN}++;
        push(@{$gene{TN}},$id);
    }
    
    elsif ($v_tir == 1                                       and $v_nbs + $v_cc + $v_rpw == 0) {
        $total{TX}++;
        push(@{$gene{TX}},$id);
    }
    
    elsif ($v_cc == 1  and  $v_nbs == 1                      and $v_lrr + $v_tir + $v_rpw == 0) {
        $total{CN}++;
        push(@{$gene{CN}},$id);
    }

    elsif ($v_rpw == 1 and $v_nbs == 1 and $v_lrr == 1 ) {
        push(@{$gene{RNL}}, $id);
    }

    elsif ($v_rpw == 1 and $v_nbs == 1 and $v_lrr == 0) {
        push(@{$gene{RN}}, $id);
    }

    elsif ($v_rpw == 1 and $v_nbs == 0 and $v_lrr == 0) {
        push(@{$gene{RPW8}}, $id);  # this group will only contain RPW8 domain without NBS
    }

#   other type summary    
    else {
        $total{other}++;
        push(@{$gene{OTHER}}, $id);
        
        # For debug purpose
        print OTHER "$_\t";
        print OTHER join("|", $v_nbs.".N", $v_lrr.".L", $v_cc.".C", $v_tir.".T", $v_rpw.".R", $v_nbs + $v_lrr + $v_cc + $v_tir + $v_rpw,"\n");
    }
}

close IN;

foreach my $type (sort {$a cmp $b} keys %total) {
    #print "$type\t$total{$type}\n";
}

foreach my $type (keys %gene) {
    # As the gene containing only rpw8 can't be grouped into NB-ARC encoding gene family, thus we export it into another family as rpw8.candidates.lst
    if ($type =~ /RPW8/i) {
        my @idlist = @{$gene{$type}};
        foreach my $id (@idlist) {
            print RPW8 "$id\t$type\n";
        }        
    }
    else {
        #containing NBS encoding, RNL, RN and OTHER types
        my @idlist = @{$gene{$type}};
        foreach my $id (@idlist) {
            print TYPE "$id\t$type\n";
        }
    }
}

close TYPE;
close OTHER;
# ---------------sub-------------------
sub domain_detect {
    my $value = shift;
    my $res = 0;
    if ($value =~ /domain/i) {
        $res = 1;
    }
    return $res;
}

sub Ptime{
          my $time = localtime;
          my ($msg)= @_;
          print STDERR "$time: $msg\n";
}